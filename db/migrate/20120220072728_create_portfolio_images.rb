class CreatePortfolioImages < ActiveRecord::Migration
  def change
    create_table :portfolio_images do |t|
      t.integer           :portfolio_id
      t.has_attached_file :attach
      t.integer           :attach_width
      t.integer           :attach_height
      t.timestamps
    end
  end
end
