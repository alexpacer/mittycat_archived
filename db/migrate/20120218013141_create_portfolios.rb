class CreatePortfolios < ActiveRecord::Migration
  def change
    create_table :portfolios do |t|
      t.string            :callname
      t.string            :subject
      t.text              :description
      t.integer           :order
      t.has_attached_file :linkimg
      t.timestamps
    end
    
  end
end
