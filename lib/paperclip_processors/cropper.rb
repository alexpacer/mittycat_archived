module Paperclip
  class Cropper < Thumbnail
    def transformation_command
      trans = []
      
      # image has been cropped by user!
      if crop_command  
        super.join(' ').sub(/ -crop \S+/, '').split(' ').each{|c| trans << c }
        trans << crop_command << "+repage"
        #trans << "-resize" << "\"75x75\""
        
        puts "----super--"
        puts super.inspect
        puts "----crop_command--"
        puts trans.inspect
        puts "------------------->"
        
        trans
      else
        if super.join(' ').include?('490x368>')
          # override the large thumbnail
          puts super.inspect
          return ["-resize", "\"75x\"", "-crop", "\"75x75+0+15\"", "+repage"]
        else
          return super
        end
        
      end
    end
    
    def crop_command
      target = @attachment.instance
      if target.cropping?
        " -crop \"#{target.crop_w.to_i}x#{target.crop_h.to_i}+#{target.crop_x.to_i}+#{target.crop_y.to_i}\" "
      end
    end
  end
end