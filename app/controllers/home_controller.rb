class HomeController < ApplicationController
  layout 'home'
  
  def welcome
  end
  
  def about
  end
  
  def portfolio
    @portfolios = Portfolio.find_all_ordered
    if !params[:callname].presence
      @default_portfolio = Portfolio.first
    else
      @default_portfolio = Portfolio.find_by_callname(params[:callname])
    end
  end
  
  def blog
  end
  
  def contact
  end
  
  def lorem_ipsum
  end
  
end
