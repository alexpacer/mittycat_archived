require 'common_module'

class Mittycat::HomeController < ApplicationController
  layout 'mittycat'
  include CommonModule
  before_filter :authenticate_admin!
  
  def welcome
  end
  
  def getcallname
    callname = create_callname(params[:text])
    respond_to do |format|
      format.json{ render :json => { :callname => callname } }
    end
  end
  
end
