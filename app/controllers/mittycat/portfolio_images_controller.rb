class Mittycat::PortfolioImagesController < ApplicationController
  layout 'layouts/mittycat'
  before_filter :authenticate_admin!
  
  # Shows all attached files  
  def create
    @image = PortfolioImage.create!(params[:portfolio_image])
  rescue ActiveRecord::RecordInvalid => e
    @error = e
  ensure
    respond_to do |format|
      format.js
    end
  end
  
  # Delete portfolio image
  def delete
    @image = PortfolioImage.destroy(params[:portfolio_image_id])
  rescue Exception => e
    @error = e
  ensure
    respond_to do |format|
      format.js
    end
  end
  
  # Image cropping UI
  def cropping
    @image = PortfolioImage.find(params[:portfolio_image_id])
  end
  
  def crop
    @image = PortfolioImage.find(params[:portfolio_image_id])
    @image.update_attributes(params[:portfolio_image])
    
    # redirect back to portfolio page
    redirect_to edit_mittycat_portfolio_path(params[:id])
  end
  
  #shows all sizes of the attachement image
  def allsizes
    @image = PortfolioImage.find(params[:portfolio_image_id])
  end
  
end
