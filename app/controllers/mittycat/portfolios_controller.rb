class Mittycat::PortfoliosController < ApplicationController
  layout 'mittycat'
  before_filter :authenticate_admin!
    
  def index
    @portfolios = Portfolio.all
  end 
  
  def new
    @portfolio = Portfolio.new
  end
  
  def create
    @portfolio = Portfolio.create!(params[:portfolio])
    
    flash[:notice] = "Portfolio has been created successfully."
    redirect_to mittycat_portfolios_path
  rescue ActiveRecord::RecordNotSaved => e
    @error = e
    respond_to do |format|
      format.js
    end
  end
  
  def edit
    @portfolio = Portfolio.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:error] = "[Exception] Unable to find record!"
    respond_to do |format|
      format.html{ redirect_to(mittycat_portfolios_path) }
    end
  end
  
  def update
    @portfolio = Portfolio.find(params[:id])
    @portfolio.update_attributes(params[:portfolio])
    @portfolio.save
    
    flash[:notice] = "Portfolio has been updated successfully"
    redirect_to mittycat_portfolios_path
  rescue
    respond_to do |format|
      format.html{ render :text => 'AAAAAAAAAAAAAAAAAAAARGGGGGGGGGGGG' }
    end
  end
  
  def destroy
    @portfolio = Portfolio.destroy(params[:id])
    redirect_to mittycat_portfolios_path
  end
  
  ## Handles attachments


  
end
