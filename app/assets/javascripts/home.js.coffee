//= require jquery.mousewheel
//= require jquery.jscrollpane.min
//= require mwheelIntent.js

$ ->
    portfolio =
        init: ()->
            $('#portfolio_holder>img').load(()->
                portfolio.initScrollPane()
            )
            $('img.thumbnail').click(()->
                $("#center_image").addClass("loading")
                
                original_url = $(this).attr('dir')
                original_width = $(this).parents('li').attr('dir').split('x')[0]
                original_height = $(this).parents('li').attr('dir').split('x')[1]
                portfolio.setThumbToCurrent($(this).attr('id'))
                portfolio.loadImage(original_width, original_height, original_url)
            )
        setThumbToCurrent: (current_id)->
            $('.thumbnail', $('.thumbs')).removeClass('current')
            $('#'+current_id).addClass('current')
        loadImage: (width, height, url)->
            # Prepare elements to addinto page
            target = $('<div id="portfolio_holder" class="scroll-pane loading-bg" />')
            new_image = $('<img />').attr('style', 'display:none;').attr('src', url).attr('width', width).attr('height',height)
            new_image.load(()->
                $(this).fadeIn('fast')
                target.removeClass('loading-bg')
                # reinitialize scrollpane
                portfolio.initScrollPane()
            )
            # replace portfolio holder with new one
            $('#portfolio_holder').fadeOut('fast', ()->
                $(this).remove()
                $('#album_cover').append(target)
                target.append(new_image)
                portfolio.initScrollPane()
            )
        initScrollPane: ()->
            $('#portfolio_holder').jScrollPane({showArrows:true, horizontalGutter:10})
    portfolio.init()