
$ ->
  portfolio_image =
    init: ()->
      #Initialize cropping 
      $('#cropbox').Jcrop({
        onChange: portfolio_image.update_crop,
        onSelect: portfolio_image.update_crop,
        #setSelect: [0, 0, 500, 500],
        aspectRatio: 1
      })
    # Update Cropping area 
    update_crop: (coords)->
      rx = 75/coords.w
      ry = 75/coords.h
      largeWidth = $('#large_width').val()
      largeHeight = $('#large_height').val()
      origWidth = $('#original_width').val()
      origHeight = $('#original_height').val()
      
      $('#preview').css({
        width: Math.round(rx * largeWidth) + 'px',
        height: Math.round(ry * largeHeight) + 'px',
        marginLeft: '-' + Math.round(rx * coords.x) + 'px',
        marginTop: '-' + Math.round(ry * coords.y) + 'px'
      })
      
      ## It actully works better without ratio....LOL
      $("#crop_x").val(Math.round(coords.x));
      $("#crop_y").val(Math.round(coords.y));
      $("#crop_w").val(Math.round(coords.w));
      $("#crop_h").val(Math.round(coords.h));
      #ratio = origWidth / origHeight
      #$("#crop_x").val(Math.round(coords.x * ratio));
      #$("#crop_y").val(Math.round(coords.y * ratio));
      #$("#crop_w").val(Math.round(coords.w * ratio));
      #$("#crop_h").val(Math.round(coords.h * ratio));
  portfolio_image.init()