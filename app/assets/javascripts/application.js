// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require jquery.remotipart
//= require rails.validations
// require_tree .

$(document).ready(function(){
	$('.flash_error').addClass('ui-state-highlight');
    
    /* initialize jQuery-UI themes */
    $('a.button').button();
    $('input.submit').button({ icons: {
                primary: "ui-icon-disk", secondary: "ui-icon-triangle-1-s"
            }});
});