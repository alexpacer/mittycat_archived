module ApplicationHelper
  
  # got this code snippet from [http://ianpurton.com/helper-to-display-rails-flash-messages]
  # handles flash message
  def flash_helper
    f_names = [:notice, :warning, :message, :error]
    fl = ''
    for name in f_names
      if flash[name]
        fl = fl + "<div class='flash_#{name} ui-state-error ui-corner-all'>" + "<span class='ui-icon ui-icon-info' style='float: left; margin-right: .3em;'></span>" + "#{flash[name]}</div>"
      end
      flash[name] = nil;
    end
    return fl.html_safe
  end
  
end
