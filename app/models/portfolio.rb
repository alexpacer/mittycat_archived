class Portfolio < ActiveRecord::Base
  has_attached_file :linkimg,{
      :storage => 's3',
      :bucket => 'mittycat',
      :s3_credentials => {
        :access_key_id => ENV['S3_KEY'],
        :secret_access_key => ENV['S3_SECRET']
      },
      :path => "/#{Rails.env}_#{ENV['DEV_CODENAME']}/clips/portfolios/:styles/:id/:filename",
      :default_url => '/assets/image_not_available.jpg'
    }
  has_many :portfolio_images
  
  validates_attachment_size :linkimg, :less_than => 200.kilobyte
  validates_presence_of :subject
  validates_presence_of :callname
  validates :callname, :uniqueness => true
  validates_attachment_content_type :linkimg, :content_type => ['image/jpeg', 'image/png', 'image/gif', 'image/jpg']
  validate :file_dimensions, :unless => "errors.any?"
  
  before_save :file_dimensions
  
  def file_dimensions
    tempfile = self.linkimg.queued_for_write[:original]
    
    unless tempfile.nil?
      dimension = Paperclip::Geometry.from_file tempfile
      if dimension.width > 60 || dimension.height > 60
        raise ActiveRecord::RecordNotSaved, 'Width or height must be at least 60px'
      end
    end  
  end
  
  
  # Find record by callname
  def self.find_by_callname(callname)
    self.where(:callname => callname).first
  end
  
  # Finds all records in ordered sequences
  def self.find_all_ordered
    self.order('"order" DESC').all
  end
  
end
