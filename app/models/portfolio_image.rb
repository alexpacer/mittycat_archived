class PortfolioImage < ActiveRecord::Base
  belongs_to :portfolios
  has_attached_file :attach,
    {
      :styles => { 
        :large => "700x400>", 
        :portfolio_cover => "490x368#", 
        :thumbnail => "490x368>", 
        :tiny => '40x40#'
      },
      :storage => 's3',
      :bucket => 'mittycat',
      :s3_credentials => {
        :access_key_id => ENV['S3_KEY'],
        :secret_access_key => ENV['S3_SECRET']
      },
      :path => "/#{Rails.env}_#{ENV['DEV_CODENAME']}/clips/portfolio_images/:styles/:id/:filename",
      :default_url => '/assets/image_not_available.jpg',
      :processors => [:cropper]
    }
  attr_accessor :crop_x, :crop_y, :crop_w, :crop_h
  after_update :reprocess_image, :if => :cropping?
  
  # Validation rules
  validates_attachment_presence :attach
  validates_attachment_size :attach, :less_than => 1.megabyte
  validates_attachment_content_type :attach, :content_type => ['image/jpeg', 'image/png', 'image/gif', 'image/jpg']
  before_save :get_dimensions
  
  # Determines if the image has been cropped or not
  def cropping?
    !crop_x.blank? && !crop_y.blank? && !crop_w.blank? && !crop_h.blank?
  end  
  
  # gets the image geometry
  def image_geometry(style = :original)
    @geometry ||= {}
    @geometry[style] ||= Paperclip::Geometry.from_file(attach.url(style))
  end
  
  private
    # Reprocess attachment 
    def reprocess_image
      attach.reprocess!(:thumbnail)
    end
    
    def get_dimensions
      tempfile = self.attach.queued_for_write[:original]
      
      unless tempfile.nil?
        dimension = Paperclip::Geometry.from_file tempfile
        self.attach_width = dimension.width
        self.attach_height = dimension.height
      end
    end
  
  
end
