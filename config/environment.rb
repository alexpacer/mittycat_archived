# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
Mittycat::Application.initialize!


# This may solve problems of
#   ** "The bucket you are attempting to access must be addressed using the specified endpoint. Please send all future requests to this endpoint."
# When trying to put objects in a non US Standard Amazon S3 bucket
# [http://www.conandalton.net/2011/02/paperclip-s3-and-european-buckets.html]
#AWS::S3::DEFAULT_HOST = "s3-ap-southeast-1.amazonaws.com"